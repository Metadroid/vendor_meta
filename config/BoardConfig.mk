include vendor/meta/config/BoardConfigKernel.mk

ifeq ($(BOARD_USES_QCOM_HARDWARE),true)
include vendor/meta/config/BoardConfigQcom.mk
endif

include vendor/meta/config/BoardConfigSoong.mk
